document.addEventListener("DOMContentLoaded", () => {
  //   Stat Collection
  const stats = [
    {
      id: "appearances",
      title: "Appearances",
      valueGetter: (data) => getStatFromDataSet(data.stats, "appearances"),
    },
    {
      id: "goals",
      title: "Goals",
      valueGetter: (data) => getStatFromDataSet(data.stats, "goals"),
    },
    {
      id: "assists",
      title: "Assists",
      valueGetter: (data) => getStatFromDataSet(data.stats, "goal_assist"),
    },
    {
      id: "goalspermatch",
      title: "Goals per match",
      valueGetter: (data) => {
        const appearances = getStatFromDataSet(data.stats, "appearances");
        const goals = getStatFromDataSet(data.stats, "goals");
        return (goals / appearances).toFixed(2);
      },
    },
    {
      id: "passesperminute",
      title: "Passes per minute",
      valueGetter: (data) => {
        const mins_played = getStatFromDataSet(data.stats, "mins_played");
        const fwd_pass = getStatFromDataSet(data.stats, "fwd_pass");
        const backward_pass = getStatFromDataSet(data.stats, "backward_pass");
        return ((fwd_pass + backward_pass) / mins_played).toFixed(2);
      },
    },
  ];

  // Vars
  let playerData = null;
  let selectedPlayer = null;
  // Dom
  const playerSelect = document.getElementById("select-player");

  //   Event Listeners
  playerSelect.addEventListener("change", (selection) => {
    selectedPlayer = playerData.find(
      (p) => p.player.id == selection.target.value
    );
    updateUi(selectedPlayer);
  });

  const addStatElement = (id, title, value) => {
    // select the list element
    const ul = document.getElementById("player-stats-list");
    // create the li and the value span
    const li = document.createElement("li");
    // add an id to the element
    li.setAttribute("id", id);
    const valueSpan = document.createElement("span");
    valueSpan.classList.add("value");
    valueSpan.setAttribute("id", `${id}-value`);
    // Add data to the elements
    valueSpan.appendChild(document.createTextNode(value));
    li.appendChild(document.createTextNode(title));
    li.appendChild(valueSpan);
    ul.appendChild(li);
  };

  const getStatFromDataSet = (stats, key) => {
    return stats.find((stat) => stat.name === key)?.value || 0;
  };

  const updateUi = (player) => {
    //   Player Info
    document.getElementById("playerName").innerText =
      playerNameFormatter(player);
    document.getElementById("playerPosition").innerText =
      playerPositionFormatter(player);
    document.getElementById(
      "playerImg"
    ).src = `assets/img/player-stats-widget/img/p${player.player.id}.png`;
    document
      .getElementById("playerImg")
      .setAttribute("alt", playerNameFormatter(player));
    //   Team
    document.getElementById("teamBadge").style.backgroundPosition =
      badgeDictionary(player.player.currentTeam.id);
    // Player Stats
    stats.forEach((stat) => {
      const valueElm = document.getElementById(`${stat.id}-value`);
      valueElm.innerText = stat.valueGetter(player);
    });
  };

  const playerNameFormatter = (p) => {
    const firstName = p.player.name.first;
    const lastName = p.player.name.last;
    return `${firstName} ${lastName}`;
  };

  const playerPositionFormatter = (p) => {
    const positionKey = p.player.info.position;
    switch (positionKey) {
      case "D":
        return "Defender";
      case "F":
        return "Forward";
      case "M":
        return "Midfield";
      default:
        return positionKey;
    }
  };

  const badgeDictionary = (teamId) => {
    switch (teamId) {
      case 21:
        return "-500px -1000px";
      case 11:
        return "-800px -700px";
      case 12:
        return "-600px -800px";
      case 1:
        return "-100px -100px";
      case 26:
        return "0px 0px";
    }
  };

  const buildDropdown = (playerData) => {
    for (let i = 0; i < playerData.length; i++) {
      const p = playerData[i];
      const element = document.createElement("option");
      element.textContent = playerNameFormatter(p);
      element.value = p.player.id;
      playerSelect.appendChild(element);
    }
  };

  const getData = (url) => {
    fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((data) => {
        playerData = data.players;
        buildDropdown(playerData);
        selectedPlayer = playerData[0];
        updateUi(selectedPlayer);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
  };

  //   Build initial UI from the stats config.
  stats.forEach((stat) => addStatElement(stat.id, stat.title, 0));
  //   Fetch data from server.
  getData("/player-stats.json");
});
