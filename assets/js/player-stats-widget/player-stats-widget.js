"use strict";

document.addEventListener("DOMContentLoaded", function () {
  //   Stat Collection
  var stats = [{
    id: "appearances",
    title: "Appearances",
    valueGetter: function valueGetter(data) {
      return getStatFromDataSet(data.stats, "appearances");
    }
  }, {
    id: "goals",
    title: "Goals",
    valueGetter: function valueGetter(data) {
      return getStatFromDataSet(data.stats, "goals");
    }
  }, {
    id: "assists",
    title: "Assists",
    valueGetter: function valueGetter(data) {
      return getStatFromDataSet(data.stats, "goal_assist");
    }
  }, {
    id: "goalspermatch",
    title: "Goals per match",
    valueGetter: function valueGetter(data) {
      var appearances = getStatFromDataSet(data.stats, "appearances");
      var goals = getStatFromDataSet(data.stats, "goals");
      return (goals / appearances).toFixed(2);
    }
  }, {
    id: "passesperminute",
    title: "Passes per minute",
    valueGetter: function valueGetter(data) {
      var mins_played = getStatFromDataSet(data.stats, "mins_played");
      var fwd_pass = getStatFromDataSet(data.stats, "fwd_pass");
      var backward_pass = getStatFromDataSet(data.stats, "backward_pass");
      return ((fwd_pass + backward_pass) / mins_played).toFixed(2);
    }
  }]; // Vars

  var playerData = null;
  var selectedPlayer = null; // Dom

  var playerSelect = document.getElementById("select-player"); //   Event Listeners

  playerSelect.addEventListener("change", function (selection) {
    selectedPlayer = playerData.find(function (p) {
      return p.player.id == selection.target.value;
    });
    updateUi(selectedPlayer);
  });

  var addStatElement = function addStatElement(id, title, value) {
    // select the list element
    var ul = document.getElementById("player-stats-list"); // create the li and the value span

    var li = document.createElement("li"); // add an id to the element

    li.setAttribute("id", id);
    var valueSpan = document.createElement("span");
    valueSpan.classList.add("value");
    valueSpan.setAttribute("id", "".concat(id, "-value")); // Add data to the elements

    valueSpan.appendChild(document.createTextNode(value));
    li.appendChild(document.createTextNode(title));
    li.appendChild(valueSpan);
    ul.appendChild(li);
  };

  var getStatFromDataSet = function getStatFromDataSet(stats, key) {
    var _stats$find;

    return ((_stats$find = stats.find(function (stat) {
      return stat.name === key;
    })) === null || _stats$find === void 0 ? void 0 : _stats$find.value) || 0;
  };

  var updateUi = function updateUi(player) {
    //   Player Info
    document.getElementById("playerName").innerText = playerNameFormatter(player);
    document.getElementById("playerPosition").innerText = playerPositionFormatter(player);
    document.getElementById("playerImg").src = "assets/img/player-stats-widget/img/p".concat(player.player.id, ".png");
    document.getElementById("playerImg").setAttribute("alt", playerNameFormatter(player)); //   Team

    document.getElementById("teamBadge").style.backgroundPosition = badgeDictionary(player.player.currentTeam.id); // Player Stats

    stats.forEach(function (stat) {
      var valueElm = document.getElementById("".concat(stat.id, "-value"));
      valueElm.innerText = stat.valueGetter(player);
    });
  };

  var playerNameFormatter = function playerNameFormatter(p) {
    var firstName = p.player.name.first;
    var lastName = p.player.name.last;
    return "".concat(firstName, " ").concat(lastName);
  };

  var playerPositionFormatter = function playerPositionFormatter(p) {
    var positionKey = p.player.info.position;

    switch (positionKey) {
      case "D":
        return "Defender";

      case "F":
        return "Forward";

      case "M":
        return "Midfield";

      default:
        return positionKey;
    }
  };

  var badgeDictionary = function badgeDictionary(teamId) {
    switch (teamId) {
      case 21:
        return "-500px -1000px";

      case 11:
        return "-800px -700px";

      case 12:
        return "-600px -800px";

      case 1:
        return "-100px -100px";

      case 26:
        return "0px 0px";
    }
  };

  var buildDropdown = function buildDropdown(playerData) {
    for (var i = 0; i < playerData.length; i++) {
      var p = playerData[i];
      var element = document.createElement("option");
      element.textContent = playerNameFormatter(p);
      element.value = p.player.id;
      playerSelect.appendChild(element);
    }
  };

  var getData = function getData(url) {
    fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    }).then(function (response) {
      return response.json();
    }).then(function (data) {
      playerData = data.players;
      buildDropdown(playerData);
      selectedPlayer = playerData[0];
      updateUi(selectedPlayer);
    })["catch"](function (error) {
      console.error("Error:", error);
    });
  }; //   Build initial UI from the stats config.


  stats.forEach(function (stat) {
    return addStatElement(stat.id, stat.title, 0);
  }); //   Fetch data from server.

  getData("/player-stats.json");
});