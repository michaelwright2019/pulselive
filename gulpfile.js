const gulp = require("gulp");
const image = require("gulp-image");
const babel = require("gulp-babel");
const sass = require("gulp-sass")(require("sass"));
const browserSync = require("browser-sync").create();

const styles = "./src/**/*.scss";
const images = "./src/**/img/*.png";
const js = "./src/**/*.js";

function buildSass() {
  return gulp
    .src(styles)
    .pipe(sass().on("error", sass.logError))
    .pipe(gulp.dest("./assets/css"))
    .pipe(browserSync.stream());
}

function buildJs() {
  return gulp
    .src(js)
    .pipe(
      babel({
        presets: ["@babel/preset-env"],
      })
    )
    .pipe(gulp.dest("./assets/js"));
}

function optimiseImgs() {
  return gulp.src(images).pipe(image()).pipe(gulp.dest("./assets/img"));
}

function serve() {
  browserSync.init({
    server: {
      baseDir: "./",
    },
  });
  gulp.watch(images, optimiseImgs);
  gulp.watch(styles, buildSass);
  gulp.watch(js, buildJs).on("change", browserSync.reload);
  gulp.watch("./index.html").on("change", browserSync.reload);
}

function init() {
  buildSass();
  buildSass();
  optimiseImgs();
  return Promise.resolve();
}

exports.buildSass = buildSass;
exports.buildJs = buildJs;
exports.optimiseImgs = optimiseImgs;
exports.serve = serve;
exports.build = init
exports.init = init;
