# Pulse Live Frontend Development Task

A Player Stats Card component to display stats for a selected player.

The build process is handled by Node.

The component was designed to be somewhat responsive in terms of viewport width.

## Demo

[https://infallible-benz-d21602.netlify.app/](https://infallible-benz-d21602.netlify.app/)


## Installation

Use the Node Package Manager (NPM) to dev dependencies.

```node
npm install
```

## Gulp Tools

```node

# To run the project locally for development: 
gulp serve

# To build the project
gulp build

```

## License
[MIT](https://choosealicense.com/licenses/mit/)